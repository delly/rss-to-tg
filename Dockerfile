FROM python:3.7-slim
WORKDIR /app
COPY . /app
RUN pip install --trusted-host pypi.python.org -r /app/requirements.txt
ENV TOKEN X
ENV CHATID X
ENV DELAY 60
CMD ["python", "telegramRSSbot.py"]